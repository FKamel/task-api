﻿using Microsoft.AspNetCore.Identity;
using System;
using System.ComponentModel.DataAnnotations;

namespace HandyAPI.Models
{
    public class AppUser : IdentityUser
    {
        public AppUser()
        {
        }  
        public string Password { get; set; }

        [Compare("Password")]
        public string confirmPassword { get; set; }
    }
}