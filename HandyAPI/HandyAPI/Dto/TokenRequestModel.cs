﻿using System.ComponentModel.DataAnnotations;

namespace HandyAPI.Dto
{
    public class TokenRequestModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}