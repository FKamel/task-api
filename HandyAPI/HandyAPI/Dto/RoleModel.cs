﻿using System.ComponentModel.DataAnnotations;

namespace HandyAPI.Dto
{
    public class RoleModel
    {
        public string Email { get; set; }
        public string Role { get; set; }
    }
}