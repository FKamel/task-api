﻿

namespace HandyAPI.Configuration
{
    public class UserData
    {
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}
