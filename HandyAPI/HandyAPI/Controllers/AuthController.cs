﻿using HandyAPI.Configuration;
using HandyAPI.Dto;
using HandyAPI.Services;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace HandyAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        private readonly IEmailService _emailService;
        public AuthController(IAuthService authService, IEmailService emailService)
        {
            _authService = authService;
            _emailService = emailService;
        }

        [HttpPost("Register")]
        public async Task<IActionResult> RegisterAsync([FromBody] RegisterModel model, bool isAdmin = false)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _authService.RegisterAsync(model, isAdmin);

            if (!result.IsAuthenticated)
                return BadRequest(result.Message);

            return Ok(result);
        }

        [HttpPost("Login")]
        public async Task<IActionResult> GetTokenAsync([FromBody] TokenRequestModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _authService.GetTokenAsync(model);

            if (!result.IsAuthenticated)
                return BadRequest(result.Message);

            return Ok(result);
        }
        [HttpPost("EmailConfrimation")]
        public async Task<IActionResult> EmailConfrimationAsync(ConfirmEmailModel model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await _authService.ConfirmEmailAsync(model);
            return Ok(result);
        }
        [HttpPost("SendMessageToEmail")]
        public bool SendMessageToEmail([FromForm] UserData userData)
        {
            return _emailService.SendMessageToConfirmEmail(userData);
        }
        [HttpPut("ResetPassword")]
        public async Task<AuthModel> ResetPassword([FromForm] ResetPasswordModel model)
        {
            return await _authService.ResetPasswordAsync(model);
        }
    }
}