﻿using Microsoft.Extensions.Options;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;
using HandyAPI.Services;
using HandyAPI.Configuration;

namespace HandyAPI.Services
{
    public class EmailService : IEmailService
    {
        EmailSettings _emailSettings = null;
        public EmailService(IOptions<EmailSettings> options)
        {
            _emailSettings = options.Value;
        }
        public bool SendMessageToConfirmEmail(UserData userData)
        {
            try
            {
                var fromAddress = new MailAddress("handyteam98@gmail.com", "Website");
                var toAddress = new MailAddress(userData.Email, userData.UserName);
                const string fromPassword = "123456789handy123456789handy";
                const string subject = "Website";
                const string body = "Click here to Confrim Your Email";
                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}