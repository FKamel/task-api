﻿using HandyAPI.Configuration;
using System.Threading.Tasks;

namespace HandyAPI.Services
{
    public interface IEmailService
    {
        public bool SendMessageToConfirmEmail(UserData userData);
    }
}
