﻿using System.Threading.Tasks;
using HandyAPI.Dto;
using HandyAPI.Models;

namespace HandyAPI.Services
{
    public interface IAuthService
    {
        Task<AuthModel> RegisterAsync(RegisterModel model, bool isAdmin);
        Task<AuthModel> GetTokenAsync(TokenRequestModel model);
        Task<AuthModel> ConfirmEmailAsync(ConfirmEmailModel model);
        Task<AuthModel> ResetPasswordAsync(ResetPasswordModel model);
    }
}